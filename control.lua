--[[
Copyright 2018 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details
	
	KPerms - Permission superset for Factorio.

--]]

require 'lib/event_extend'

require 'kperms/kperms'
require 'kperms/api'
require 'kperms/cmd'
