--[[
Copyright 2018 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details
	
--]]

remote.add_interface("kperms", {
	register_mod_permission = kperms.registerPerms, -- modname, permname
	list_mod_permissions = kperms.listCustomPerms, -- (modname)
	list_mods_with_permissions = kperms.listModsWithPerms,
	
	player_has_permission = kperms.playerHasPerm, -- player, modname, permname
	group_has_permission = kperms.groupHasPerm, -- groupname, modname, permname
	
	create_group = kperms.createGroup, -- groupname
	delete_group = kperms.deleteGroup, -- groupname
	rename_group = kperms.renameGroup, -- currentGroupName, newGroupName
	list_groups = kperms.groups, -- -
	group_permissions = kperms.groupPermissions, -- groupname
	add_group_permission = kperms.addGroupPerm, -- groupname, mod, permname
	remove_group_permission = kperms.removeGroupPerm, -- groupname, mod, permname
	set_default_group = kperms.setDefaultGroup, -- groupname
	
	set_player_group = kperms.setPlayerGroup, -- player, groupname

	events = kperms.getEvent, -- event_name
})
