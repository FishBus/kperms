--[[
Copyright 2018 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details
	
--]]

require 'lib/player_helper'
require 'lib/fishbus_util'

kperms = {}
local events = {
	on_add_group_permission = Event.def("on_add_group_permission"),
	on_group_created = Event.def("on_group_created"),
	on_group_deleted = Event.def("on_group_deleted"),
	on_group_renamed = Event.def("on_group_renamed"),
	on_permission_registered = Event.def("on_permission_registered"),
	on_set_player_group = Event.def("on_set_player_group"),
	on_remove_group_permission = Event.def("on_remove_group_permission"),
}

function kperms.init(event)
	if not global.kperms then
		log("kperms.init")
		global.kperms = {
			mod_permissions = {},
			groups = {},
			players = {},
			defaultGroup = nil,
			events = events,
		}
	end
	if not kperms.defaultGroup() then
		kperms.createGroup("Default")
		kperms.setDefaultGroup("Default")
	end
end

function registerPerm(modname, permname)
	local mp = global.kperms.mod_permissions
	if not mp[modname] then
		mp[modname] = {}
	end
	if not arr_contains(mp[modname], permname) then
		table.insert(mp[modname], permname)
		Event.on_next_tick(function() 
			script.raise_event(global.kperms.events.on_permission_registered, {
				modname = modname,
				permission = permname,
			})
		end)
		return true
	end
	return false
end

function kperms.registerPerms(modname, perms)
	if type(modname) ~= "string" then
		return {false, "modname not valid"}
	end
	local count = 0
	if type(perms) == "string" then
		if registerPerm(modname, perms) then
			count = count + 1
		end
	elseif type(perms) == "table" then
		for idx, permname in ipairs(perms) do
			if type(permname) == "string" then
				if registerPerm(modname, permname) then
					count = count + 1
				end
			else
				log({'kperms.registerPerm_badname', modname, serpent.line(permname)})
			end
		end
	end
	return {true, count}
end

function kperms.listCustomPerms(modname)
	if modname then
		return table.deepcopy(global.kperms.mod_permissions[modname])
	end
	return table.deepcopy(global.kperms.mod_permissions)
end

function kperms.listModsWithPerms()
	local modlist = {}
	for modname, perms in pairs(global.kperms.mod_permissions) do
		table.insert(modlist, modname)
	end
	return modlist
end

function kperms.playerHasPerm(playerdata, modname, permname)
	local player = getPlayer(playerdata)
	if player then
		local groupname = global.kperms.players[player.index]
		if groupname then
			return kperms.groupHasPerm(groupname, modname, permname)
		end
	end
	return false
end

function kperms.groupHasPerm(groupdata, modname, permname)
	local group = kperms.group(groupdata)
	if group and group.permissions[modname] and arr_contains(group.permissions[modname], permname) then
		return true
	end
	return false
end

function kperms.createGroup(groupname)
	if type(groupname) ~= "string" then
		return {false, "groupname must be string"}
	end
	if global.kperms.groups[groupname] then
		return {false, "groupname exists"}
	end
	global.kperms.groups[groupname] = {
		kperms_group = true,
		name = groupname,
		players = {},
		permissions = {},
	}
	Event.on_next_tick(function() 
		script.raise_event(global.kperms.events.on_group_created, {
			name = groupname,
		})
	end)
	return {true, groupname}
end

function kperms.deleteGroup(groupname)
	local group = kperms.group(groupname)
	if not group then
		return {false, 'group not found'}
	end
	local defaultGroup = kperms.defaultGroup()
	if defaultGroup == group.name then
		return {false, "cannot delete default group"}
	end
	-- move players to default group before deleting
	for _, plidx in ipairs(group.players) do
		kperms.setPlayerGroup(plidx, defaultGroup)
	end
	global.kperms.groups[groupname] = nil

	Event.on_next_tick(function() 
		script.raise_event(global.kperms.events.on_group_deleted, {
			name = groupname,
		})
	end)
	return {true, groupname}
end

function kperms.renameGroup(currentGroupName, newGroupName)
	local group = kperms.group(currentGroupName)
	local check = kperms.group(newGroupName)
	if check then
		return {false, "new name taken"}
	end
	if not group then
		return {false, "group does not exist"}
	end
	group.name = newGroupName
	local glpl = global.kperms.players
	for idx = 1, #group.players do
		glpl[group.players[idx]] = group.name
	end
	script.raise_event(global.kperms.events.on_group_renamed, {
		oldname = currentGroupName,
		newname = newGroupName,
	})
	return {true}
end

function kperms.copyGroupPerms(sourcegroup, destgroup, retain)
	local src = kperms.group(sourcegroup)
	local dest = kperms.group(destgroup)
	if src and dest then
		if not retain then
			dest.permissions = {}
		end
		table.deepcopy(src.permissions, dest.permissions)
		return {true}
	end
	return {false, "group not found"}
end

function kperms.groups()
	return table.keys(global.kperms.groups)
end

function kperms.group(groupdata)
	if type(groupdata) == "string" then
		if global.kperms.groups[groupdata] then
			return global.kperms.groups[groupdata]
		end
	elseif type(groupdata) == "table" and groupdata.kperms_group then
		return groupdata
	end
	return nil
end

function kperms.groupPermissions(groupname)
	local group = kperms.group(groupname)
	if group then
		game.print("DEBUG: " .. serpent.line(group.permissions))
		return {true, table.deepcopy(group.permissions)}
	end
	return {false, "group not found"}
end

function kperms.permissionExists(modname, permname)
	if type(modname) == "string" and type(permname) == "string" then
		return arr_contains(global.kperms.mod_permissions[modname], permname)
	end
	return false
end

function kperms.addGroupPerm(groupname, modname, permname)
	local group = kperms.group(groupname)
	if kperms.permissionExists(modname, permname) then
		local gp = group.permissions
		if not gp[modname] then
			gp[modname] = { permname }
		else
			if not arr_contains(gp[modname], permname) then
				table.insert(gp[modname], permname)
			end
		end
		Event.on_next_tick(function() 
			script.raise_event(global.kperms.events.on_add_group_permission, {
				group = group.name,
				modname = modname,
				permname = permname,
			})
		end)
		return {true}
	end
	return {false, "permission not found"}
end

function kperms.removeGroupPerm(groupname, modname, permname)
	local group = kperms.group(groupname)
	if group then
		local gp = group.permissions
		if gp[modname] and arr_contains(gp[modname], permname) then
			arr_remove(gp[modname], permname)
			Event.on_next_tick(function() 
				script.raise_event(global.kperms.events.on_remove_group_permission, {
					group = group.name,
					modname = modname,
					permname = permname,
				})
			end)
			return {true}
		else
			return {false, "group permission not found"}
		end
	end
	return {false, "group not found"}
end

function kperms.defaultGroup()
	return global.kperms.defaultGroup
end

function kperms.setDefaultGroup(groupname)
	local group = kperms.group(groupname)
	if group then
		global.kperms.defaultGroup = group.name
		return {true}
	end
	return {false, "group not found"}
end

function kperms.setPlayerGroup(player_input, group_input)
	local player = getPlayer(player_input)
	local group = kperms.group(group_input)
	if player and group then
		log("DEBUG: kperms, setting player "..player.index.." to group "..group.name)
		local glpl = global.kperms.players
		if glpl.group == group.name then
			return {true}
		end
		local oldgroup = {name=nil}
		if glpl[player.index] then
			oldgroup = kperms.group(glpl[player.index])
			arr_remove(oldgroup.players, player.index)
		end
		global.kperms.players[player.index] = group.name
		table.insert(group.players, player.index)
		Event.on_next_tick(function() 
			script.raise_event(global.kperms.events.on_set_player_group, {
				player_index = player.index,
				oldgroup = oldgroup.name,
				group = group.name,
			})
		end)
		return {true}
	end
	if not player then
		return {false, "player not found"}
	end
	return {false, "group not found"}
end

function kperms.getEvent(eventname)
	if events[eventname] then
		return events[eventname]
	end
	return global.kperms.events[eventname]
end

function kperms.on_player_created(event)
	-- add player to default group.
	log("Creating player, adding to kperms group, "..kperms.defaultGroup())
	kperms.setPlayerGroup(event.player_index, kperms.defaultGroup())
end

function kperms.on_player_removed(event)
	-- this is a special case, we don't want to do any lookups of this index,
	-- just wipe it out from any previous knowledge.
	local plidx = event.player_index
	local glpl = global.kperms.players
	if glpl[plidx] then
		local oldgroup = kperms.group(glpl[plidx])
		arr_remove(oldgroup.players, plidx)
	end
	glpl[plidx] = nil
end

Event.register(Event.def("soft_init"), kperms.init)
Event.register(defines.events.on_player_created, kperms.on_player_created)
Event.register(defines.events.on_player_removed, kperms.on_player_removed)
