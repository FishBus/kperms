--[[
Copyright 2018 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details
	
--]]

if true == false then
	-- just for debugging..
	
	commands.add_command('kperms.debug', '', function(event)
		game.print("DEBUG:")
		game.print(serpent.block(global.kperms))
	end)
	
end
