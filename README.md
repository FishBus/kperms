# KPerms

Kovus' custom Factorio permissions lib.
Enabling additional permissions to be created by mod authors, and more defined groups for game admins, primarily for use in multiplayer.


### API

There's two usage interests to KPerms:
- The mod permission registration & check; and
- The permission management (players & groups).

There's a few functions which have some overlap, so here's the full remote API.  Parameters prefixed with a `*` are optional.

`register_mod_permission(modname, permname/permlist)`  
Informs KPerms of your mods' permission(s).  If passed an array, it will register multiple permissions at once.  
Triggers `on_permission_registered`.  
Returns {false, msg} on error.  
Returns {true, count} on success, where count is the number of permissions added.

`list_mod_permissions(*modname)`  
Returns a table copy of all the mod permissions for `modname`.  
Returns table copy of all mods permissions if given no input.

`list_mods_with_permissions()`  
Returns list of all mods which have registered permissions.

`player_has_permission(player, modname, permname)`  
Returns `true` if a player is in a group with the specified mod's permission, `false` otherwise.

`group_has_permission(groupname, modname, permname)`  
Returns `true` if the group has the specified mod's permission, `false` otherwise.

`create_group(groupname)`  
Creates a new group in KPerms.  
Returns {false, reason} if group fails to be created  
Return {true, groupname} on success.  
Triggers KPerms' event `on_group_created`

`delete_group(groupname)`  
Deletes a group in KPerms.  
Returns {false, reason} if group fails to be deleted  
Return {true, groupname} on success.  
Triggers KPerms' event `on_group_deleted`

`list_groups()`  
Returns a list of all of the groups defined in KPerms.

`group_permissions(groupname)`  
Returns a table of all of the permissions given to the group named `groupname`.  
Table is formatted as `{mod1 = {perm1, perm2,}, mod2 = {perm1, perm4}}`.

`add_group_permission(groupname, mod, permname)`  
Adds the specified mod/permname permission to a group.  
Returns {status, reason} where status is true/false.

`remove_group_permission(groupname, mod, permname)`  
Removes the specified mod/permname permissions from a group.  
Returns {status, reason} where status is true/false.

`set_default_group(groupname)`  
Sets the default group a player is added to when being created.  
Returns {status, reason} where status is true/false.

`set_player_group(player, groupname)`
Set a player into a group.  Players may only be in one group at a time.  
Returns {status, reason} where status is true/false.

`events(event_name)`
Returns the event with provided name, such as `on_group_created`.

#### Events

KPerms does have some events:

```
on_permissions_registered
	modname :: string
	permission_name :: string
```
Called upon *every* permission registration.  If a mod registers multiple permissions at once, this event will trigger for each one it registers.

```
on_group_created
	name :: string
```
Called when a KPerms group is created, contains the name of the group.


#### Using
The remote API described above lists itself as
```lua
function_name(param1, param2, *param3)
```
Your code to call this function will look like this:
```lua
remote.call('kperms', 'function_name', 'param1', 'param2', 'param3')
```

#### Mod-Example

Only a portion of KPerms is really relevant to regular mod activity.  Typically this will be to register & check for players for permissions.

```lua
function mod_init()
	-- do stuff
	remote.call('kperms', 'register_mod_permission', 'mymod', 'teleport')
end

function teleport_player_to(event, target_player)
	if remote.call('kperms', 'player_has_permission', event.player_index, 'mymod', 'teleport') then
		-- teleport to the player.
	end
end
```

#### Admin-mod example

KPerms has no ability for an admin to manage it, and will rely on an administrative mod of some sort.  So, and admin mod will need to know how to interact with the group/user management, as well as needing to get information about mods which have registerd permissions.  That's most of the rest of the API.

This is on the TODO, but I may later just reference another mod, since an admin-mod would need to exist for KPerms to be truly useful.
